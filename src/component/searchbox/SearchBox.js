import React from "react";
import "./searchbox.css"
export default function SearchBar(props) {


    const input = React.useRef("");
    const getSerchItem = () => {
        props.searchKeyword(input.current.value)
    }
    return (
        <div className="search-box">
            <input ref={input} className="search-txt" type="text" placeholder="Type to search"
                value={props.term} onChange={getSerchItem}></input>
            <button className="search-btn">
                <i className="fas fa-search"></i></button>
        </div>
    )
}