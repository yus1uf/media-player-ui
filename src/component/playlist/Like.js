import React from "react";

export default function Like(props) {

    const [like, setLike] = React.useState(props.like)

    return (
        <div className="like">
            <button className="like-btn" onClick={() => setLike(like + 1)}><i class="fas fa-thumbs-up"></i></button>
            <span>{like}</span>
        </div>
    )
}