import React from 'react';
import "./addMedia.css"
export default function AddMedia(props) {

    const [status, setStatus] = React.useState(false)
    const [title, setTitle] = React.useState("")
    const [subTitle, setSubTitle] = React.useState("")

    const { data, setData } = props;
    function addData(e) {
        console.log(data)
        if (title && subTitle !== "") {
            setData([...data,
            {
                id: data.length + 1,
                like: 0,
                title: title,
                subtitle: subTitle,
                media: ""
            }]
            )
            alert("Song added to Media Player")
            setStatus(false)
        }
        e.preventDefault();
    }
    return (
        <div>

            {
                status ?
                    <div className="form">
                        <form onSubmit={addData}>
                            Title :  <input type="text" onChange={(e) => setTitle(e.target.value)}></input>
                            SubTitle : <input type="text" onChange={(e) => setSubTitle(e.target.value)}></input>
                            <button type="submit">Add</button>
                        </form>
                    </div>
                    : ""
            }
            <button onClick={() => setStatus(!status)} className="add-song">+</button>
        </div>
    )
}